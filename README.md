
# Introduction

Afin de mettre en pratique les concepts vus en cours, nous allons nous appuyer sur des jeux de données publiques hébergées par le [UC Irvine Machine Learning Repository](http://archive.ics.uci.edu/ml/index.php).

Pour le premier jeu de données intitulé *mushrooms*, il s'agit de classer un champignon comme comestible ou non en fonction d'attributs de type catégoriel. Pour le second *italian wines*, il s'agit de prédire le cultivar du cépage en fonction de mesures quantitatives.

Pour cela, nous utiliserons différents environnements :

* [`KNIME`](https://www.knime.com) : un logiciel d'analyse qui est en fait un environnement dérivé de la plateforme de développement intégré Eclipse
* `R` : environnement orienté calcul numérique & statistiques
* `Python` : un langage de programmation afin de voir l'utilisation de bibliothèque de fouille de données ainsi que pour réaliser son propre programme de classification 

## git

Pour récupérer le projet `gitlab` avec les sujets et les données :

```bash
git clone https://gitlab.com/rbarriot/datamining.git
```

Pour récupérer les dernières mises à jour, **se placer dans le répertoire du projet** (`datamining` lors du clonage) ;

```bash
git pull
```

## Environnement de travail avec `mamba` (ou `conda`)

Nous allons utiliser quelques librairies pas toujours présentes dans un environnement par défaut. C'est l'occasion de pratiquer la gestion de différents environnements de développement et/ou d'analyse. Pour cela, nous allons utiliser l'utilitaire `mamba` (plus rapide que `conda`). Plus d'informations sur mamba : [ce gitlab](https://gitlab.com/rbarriot/guides/-/tree/master/shell#gestionnaire-denvironnements) ou directement sur https://mamba.readthedocs.io.

L'environnement `fouille` devrait être disponible dans votre shell. `mamba env list` pour avoir la liste des environnements, et `mamba activate fouille` pour activer celui-ci.
<p>
<details>
<summary>création de l'environnement.</summary>

Pour créer un environnement nommé `fouille` avec tout le nécessaire pour les TP, les commandes suivantes sont à exécuter dans un terminal (**surtout PAS en tant qu'administrateur/root**).

```bash
# dépôts (channels) des librairies et programmes
conda config --add channels conda-forge 
conda config --add channels bioconda 
# création d'un nouvel environnement contenant les librairies spécifiées
mamba create -n fouille r-base r-tidyverse r-ggally r-reticulate r-rmysql bioconductor-made4 r-codetools r-caTools r-rprojroot r-plotly r-knitr r-dt r-kableextra r-cluster r-gridextra r-caret r-e1071 r-uwot  python numpy pandas scipy scikit-learn r-rmdformats matplotlib plotly ipykernel nb_conda_kernels jupyterlab auto-sklearn
# activation de ce nouvel environnement
mamba activate fouille
```
</details>
</p>

# TP Classification

* TP 2h : Exploration R puis Knime pour un arbre de décision sur *Mushrooms* → répertoire [tp.mushrooms.knime](./tp.mushrooms.knime)
* TP 4h : Analyse discriminante linéaire sur *Italian wine* → répertoire [tp.italian.wine.lda](./tp.italian.wine.lda)
* TP 4h : Classificateur bayésien naïf (jupyter lab, pandas et script) sur *Mushrooms* et/ou *Italian wine* → répertoire [tp.naive.bayes.jupyter-lab.pandas.script](./tp.naive.bayes.jupyter-lab.pandas.script)
* TP 2h : Modules python sklearn et auto-sklearn [tp.knn.jupyter.sklearn.autosklearn](./tp.knn.jupyter.sklearn.autosklearn)

# TP Clustering

* TP 4h : [Mise en oeuvre des concepts vus en cours concernant l'évaluation des résultats d'un clustering](./tp.clustering)

# Devoir maison / travail personnel

* sujet sur [dm.italian.wine.tidyverse.knime.analyses](./dm.italian.wine.tidyverse.knime.analyses/)

# Projet

* GitLab → https://gitlab.com/rbarriot/datamining.project
