# TP 2h - Classification par arbres de décision et évaluation des performances - données Mushrooms
## Les données

Le premier jeu de données concerne des champignons. Il est publié à l'adresse : http://archive.ics.uci.edu/ml/datasets/Mushroom

La description du jeu de données est aussi accessible [ici](Mushrooms.names.md).

Le jeu de données modifié pour inclure une première ligne contenant les noms des colonnes est disponible là : [mushrooms.data.csv](../data/mushrooms.data.csv).

## Analyses préliminaires avec R

Il faudra tout d'abord charger le jeu de données et utiliser la commande `summary` pour vous faire une idée du nombre d'instances de chaque classe, et du nombre de modalités de chaque facteur (attribut/dimension/variable).

La deuxième étape consiste à étudier la liaison entre chaque variable et la classe. Pour cela, on pourra utiliser le test du χ2 d'indépendance. On pourra également utiliser la fonction `table` (pour générer une table de contingence) pour la combiner avec la fonction `plot` afin d'explorer visuellement les biais entre chaque attribut et la classe.

Exemple de visualisation :

![class *vs.* gill attachment](mushrooms.gill.attachment.png)

Ces analyses devrait nous permettre de nous faire une idée sur la pertinence d'un attribut en ce qui concerne l'objectif : classer un champignon comme comestible ou pas.


Pour cette partie, suivez le tutoriel du *notebook* R [mushrooms.exploration.Rmd](mushrooms.exploration.Rmd).

## Analyses avec KNIME

Pour cette partie, nous allons utiliser l'environnement pour la fouille de données KNIME. C'est un logiciel en Java développé à l'origine par l'université de Constance (Allemagne).

L'installation pour linux consiste à télécharger et désarchiver le contenu d'un fichier au format .tar.gz. Une copie de la dernière version a sûrement été placée sur le PC de l'intervenant : http://pc-bioinfo-1d.univ-tlse3.fr

Après extraction, il faudra lancer l'exécutable en ligne de commande qui se trouve dans le nouveau répertoire knime_VERSION/knime.

## Construction du modèle

La première étape consiste à charger les données. Dans KNIME, ajoutez un noeud `File Reader `(section `IO` pour Input/Output) et configurez-le afin de charger le fichier de données.

Comme premier exercice, ajoutez un noeud `Decision Tree Learner` (induction d'arbre de décision) et connectez la sortie du `File Reader` à l'entrée du `Decision Tree Learner`. Configurez ce dernier pour qu'il cherche à prédire la classe du champignon. Lancez l'éxécution de ces noeuds et visualisez l'arbre de décision inféré. Quelles sont les variables les plus importantes ?

**Remarque :** A la configuration du noeud `Decision Tree Learner`, observez les autres paramètres disponibles (mesure de pertinence d'un attribut, élagage).

Effectuez la même chose avec un classificateur bayésien naïf et visualisez le modèle obtenu.

## Evaluation des performances

Afin de décider quelle méthode fonctionne le mieux (arbre de décision ou bayésien naïf) pour ce jeu de données et avec quels paramètres (`gain ratio` ou `gini index` par exemple), vous allez effectuer des validation croisées.

Pour cela, ajoutez et en configurez un noeud `Cross validation` (section `Meta`). Une `leave-one-out cross validation` (ou LOOCV) sera pour ce TP trop couteuse en temps (> 8000 modèles inférés par méthode). Essayez par exemple avec les valeurs 3 et 10 pour le noeud `X-partitioner` (3-fold ou 10-fold cross validation). Ceci aura pour effet de diviser le jeu de données en entrée en jeux de données d'apprentissage (pour le *learner*) et jeux de données *tests* (pour le modèle produit par le *learner*). Configurez enfin le noeud `X-agregator` qui confrontera la classe prédite à la classe connue.

Examinez ensuite le taux d'erreurs à l'aide d'un noeud `Statistics view`.

Faites varier la méthode et ces paramètres et notez à chaque fois les performances obtenues (taux d'erreurs) :

  * arbre de décision
    * gain ratio ou gini index
    * no pruning ou MDL 
  * bayésien naïf 

Qu'observez vous lorsque vous augmentez le nombre de validations croisées (3-fold vs. 10-fold) ?

**Saisie des résultats :** https://docs.google.com/spreadsheets/d/1-jodQc1frwTepwzwAvTD7qi_zMwPsj5LfKI6B4qAJKE/edit?usp=sharing 