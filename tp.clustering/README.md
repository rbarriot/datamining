# TP Clustering : Mise en oeuvre des concepts vus en cours concernant l'évaluation des résultats d'un clustering

Étapes :

  * Normalisation du jeu de données
  * Existe-t-il une structure en clusters ? Visualisation (projection *multidimensional scaling* et/ou ACP)
  * Réalisation d'un clustering
  * Évaluation non supervisée : coefficient de silhouette
  * Détermination du nombre de clusters
  * Évaluation supervisée
  * Comparaison de résultats de clustering


# Normalisation du jeu de données

Lecture du fichier :
<pre>
> tb                                                                                                                                                          
# A tibble: 178 × 14
   cultivar alcohol `malic-acid`   ash `alcalinity-of-ash` magnesium `total-phenols` flavonoids `nonflavonoid-phenols`
   <fct>      <dbl>        <dbl> <dbl>               <dbl>     <dbl>           <dbl>      <dbl>                  <dbl>
 1 C           14.2         1.71  2.43                15.6       127            2.8        3.06                   0.28
 2 C           13.2         1.78  2.14                11.2       100            2.65       2.76                   0.26
 3 C           13.2         2.36  2.67                18.6       101            2.8        3.24                   0.3 
 4 C           14.4         1.95  2.5                 16.8       113            3.85       3.49                   0.24
 5 C           13.2         2.59  2.87                21         118            2.8        2.69                   0.39
 6 C           14.2         1.76  2.45                15.2       112            3.27       3.39                   0.34
 7 C           14.4         1.87  2.45                14.6        96            2.5        2.52                   0.3 
 8 C           14.1         2.15  2.61                17.6       121            2.6        2.51                   0.31
 9 C           14.8         1.64  2.17                14          97            2.8        2.98                   0.29
10 C           13.9         1.35  2.27                16          98            2.98       3.15                   0.22
# … with 168 more rows, and 5 more variables: proanthocyanins <dbl>, `color-intensity` <dbl>, hue <dbl>,
#   `od280-od315-of-diluted-wines` <dbl>, proline <dbl>
</pre>

**Proposer et effectuer une analyse montrant la nécessité (ou pas) de normaliser les données.**

Nous allons utiliser la normalisation z-score qui correspond à centrer·réduire la matrice de données. Cette normalsiation est disponible avec la fonction `scale` de R :
<pre>
> tbz
# A tibble: 178 × 14
   cultivar alcohol `malic-acid` ash `alcalinity-of-ash` magnesium `total-phenols` flavonoids
   <fct>          <dbl>            <dbl>   <dbl>                   <dbl>         <dbl>               <dbl>          <dbl>
 1 C              1.51           -0.561    0.231                  -1.17         1.91                 0.807          1.03 
 2 C              0.246          -0.498   -0.826                  -2.48         0.0181               0.567          0.732
 3 C              0.196           0.0212   1.11                   -0.268        0.0881               0.807          1.21 
 4 C              1.69           -0.346    0.487                  -0.807        0.928                2.48           1.46 
 5 C              0.295           0.227    1.84                    0.451        1.28                 0.807          0.661
 6 C              1.48           -0.516    0.304                  -1.29         0.858                1.56           1.36 
 7 C              1.71           -0.417    0.304                  -1.47        -0.262                0.327          0.491
 8 C              1.30           -0.167    0.888                  -0.567        1.49                 0.487          0.481
 9 C              2.25           -0.623   -0.716                  -1.65        -0.192                0.807          0.952
10 C              1.06           -0.883   -0.352                  -1.05        -0.122                1.09           1.12 
# … with 168 more rows, and 6 more variables: `nonflavonoid-phenols` <dbl>, proanthocyanins <dbl>,
#   `color-intensity` <dbl>, hue <dbl>, `od280-od315-of-diluted-wines` <dbl>, proline <dbl>
</pre>

Après normalisation, les moyennes des variables doivent donc être 0, et les écarts-types 1 :
<pre>
> tbz %>% 
+   select(alcohol:proline) %>% 
+   apply(2, mean) %>%
+   round(3)
                     alcohol                   malic-acid                          ash            alcalinity-of-ash 
                           0                            0                            0                            0 
                   magnesium                total-phenols                   flavonoids         nonflavonoid-phenols 
                           0                            0                            0                            0 
             proanthocyanins              color-intensity                          hue od280-od315-of-diluted-wines 
                           0                            0                            0                            0 
                     proline 
                           0 
> tbz %>% 
+   select(alcohol:proline) %>% 
+   apply(2, sd)
                     alcohol                   malic-acid                          ash            alcalinity-of-ash 
                           1                            1                            1                            1 
                   magnesium                total-phenols                   flavonoids         nonflavonoid-phenols 
                           1                            1                            1                            1 
             proanthocyanins              color-intensity                          hue od280-od315-of-diluted-wines 
                           1                            1                            1                            1 
                     proline 
                           1 
</pre>

# Visualisation

La matrice individus-variables n'est pas toujours disponible pour réaliser une ACP afin de visualiser le jeu de données. Parfois, on ne dispose que de la matrice de distances entre individus. Il est alors possible de faire une **MDS** (mise à l'échelle multi-dimensionnelles) afin de projeter les individus dans un nouveau repère qui respecte au mieux les distances. La fonction R permettant de le faire est `cmdscale`.

→ Calculez la matrice de distances entre individus : fonction `dist`. Vous utiliserez la distance euclidienne ou une autre de votre choix.

→ Réalisez la MDS et récupérez les coordonnées des individus pour les ajouter au *tibble* et utiliser `ggplot` pour effectuer le graphique

![**Figure 1.** Visualisation du jeu de données](wine.pca.mds.png)


D'après ces graphiques et vos connaissances du jeu de données, choisissez un nombre de clusters *a priori* pour k-means dans la section suivante.

# Clustering

Après avoir choisi une valeur de k pour k-means, réalisez le clustering et affichez le résultat graphiquement (comme précédemment mais en faisant en plus apparaître les clusters).

**Remarque :** vous aurez besoin de la fonction `kmeans` de la librairie `stats`.

Pour k=3, le résultat devrait ressembler au graphique ci-dessous.

![**Figure 2.**k-means clustering sur wine pour k=3](wine.kmeans.3.png)

# Evaluation avec une mesure de qualité non supervisée : coefficient de silhouette

Il s'agit à présent de déterminer la qualité du clustering obtenu avec le coefficient de silhouette.

Vous allez donc implémenter une fonction `silhouette` qui prend en paramètres :

  * un vecteur contenant le cluster attribué à chacun des individus
  * la matrice de distances pour toutes les paires d'individus (avec les lignes et les colonnes triées dans le même ordre que le vecteur avec les clusters attribués)

La fonction devra renvoyer une liste contenant 3 éléments : 

  * `elements`: les coefficients de silhouette pour chaque individu
  * `clusters`: les coefficients de silhouette pour chaque cluster
  * `clustering`: le coefficient de silhouette pour le clustering
  
**Rappel :**

Pour un individu :

  * $`a_i`$ distance moyenne aux objets du cluster
  * $`b_i`$ minimum des distances moyennes de $`i`$ aux objets d'un autre cluster
  * $`s_i = \frac{b_i - a_i}{max(a_i,b_i)}`$

Pour un cluster : moyenne des coefficients de silhouette de ses membres

Pour un clustering ; moyenne des coefficents de tous les individus

Vous allez donc devoir déterminer les $`a_i`$ et $`b_i`$ pour chaque individu. Et pour cela, il vous faudra donc calculer la moyenne des distances de chaque objet à chaque objet de chacun des clusters. Les fonctions `tapply` et `mean` pourront vous être utiles.




Ajoutez cette information au graphique précédent comme ci-dessous. Est-ce que tous les coefficients sont positifs ?

![**Figure 3.**Visualisation des coefficents de silhouette comme degré de transparence](wine.kmeans.3.silhouette.png)

# Détermination du nombre de clusters

Effectuez le clustering avec différentes valeurs de k en déterminant à chaque fois le coefficient de silhouette afin d'obtenir une courbe comme celle ci-dessous.

Pour cela, il pourra être utile d'utiliser la fonction `lapply` ou `sapply` afin d'appeler votre fonction `silhouette` sur les clusters obtenus par les différentes valeurs de k.

![**Figure 4.**Balayage des valeurs de k pour déterminer la meilleure](wine.kmeans.silhouettes.png)

# Evaluation avec des mesures supervisées

Implémentez, à présent, des fonctions similaires à `silhouette` pour calculer la pureté et l'entropie des clusters et du clustering.

**Rappel :**

$`p_i = max _j p_{ij}`$ et $`purity=\sum_{i=1}^K \frac{n_i}{n}p_i`$ avec $`K`$ le nombre de clusters, $`p_i`$ la pureté du cluster $`i`$, $`n`$ le nombre d'objets et $`n_i`$ la taille du cluster $`i`$.

![**Figure 5.**Balayage des valeurs de k sur la pureté des clusters](wine.kmeans.purity.png)


De même pour l'entropie. 

**Rappel :**

$`e_i = - \sum_{j=1}^{L} p_{ij} \log p_{ij}`$ avec $`L`$ le nombre de classes et $`p_{ij} = n_{ij}/n_i`$ la probabilité qu'un membre du cluster $`i`$ appartienne à la classe $`j`$

$`E = \sum_{i=1}^{K} \frac{n_i}{n} e_i`$ avec $`K`$ le nombre de clusters et $`j`$ les classes


![**Figure 6.**Balayage des valeurs de k sur l'entropie des clusters](wine.kmeans.entropy.png)

**Remarque :** Pour aller plus loin(à faire par soi-même), il serait intéressant de calculer la mesure F (cf. mesures d'évaluation pour la classification).

```math
precision(i,j) = \frac{n_{ij}}{n_j}
```

```math
recall(i,j) = \frac{n_{ij}}{n_i}
```

```math
F(i,j) = \frac{2\times precision(i,j) \times recal(i,j)}{precision(i,j)+recal(i,j)}
```

```math
F = \sum_i \frac{n_i}{n}max(F(i,j))
```

# Comparaison de résultats de clustering

Dans cette partie, il s'agit de calculer la distance entre 2 résultats de partitionnement. Nous prendrons le résultat de k-means préalablement obtenu et les classes connues. Il s'agit donc de générer les matrices carrées d'appartenance à un même cluster puis de comptabiliser les accords et désaccords entre les 2 partitionnements.


![**Figure 7.** Comparaison de résultats de clustering](clustering.comparison.png)

# Liens

  * librairie R : `factoextra` https://www.rdocumentation.org/packages/factoextra
  * librairie R : `cluster` https://cran.r-project.org/web/packages/cluster/cluster.pdf
  * module python : `sklearn` https://scikit-learn.org/stable/modules/clustering.html#clustering
