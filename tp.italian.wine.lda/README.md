# TP Classification, analyse discriminante linéaire en R

## Les données

Concernant les données, il s'agit du même jeu de données que celui utilisé pour le [Devoir Maison](../dm.italian.wine.tidyverse.knime.analyses/) et vous devriez donc être déjà familier avec celui-ci.

Le jeu de données concerne des vins italiens. Il est publié à l'adresse : http://archive.ics.uci.edu/ml/datasets/Wine

La description du jeu de données est aussi accessible dans le fichier [`italian.wine.variables.md`](../dm.italian.wine.tidyverse.knime.analyses/italian.wine.variables.md).

Le jeu de données a été modifié pour inclure une première ligne (noms des colonnes) : fichier [`wine.data.csv`](../data/wine.data.csv).


# Analyse discriminante linéaire


L'analyse discriminante linéaire permet de trouver la combinaison linéaire des variables qui permet de séparer le mieux les classes. Pour cela, nous allons utiliser la librairie MASS :

```{r}
library(plotly) 
library(tidyverse)

tb = read_csv('wine.data.csv') %>%
   mutate(cultivar=as.factor( c('C','M','V'))[ cultivar ] )

mat <- tb %>% 
   select(alcohol:proline) %>% as.matrix
y <- tb$cultivar
```

Exploration de la matrice individus-variables avec une ACP :
```{r}
tb.acp <- tb %>% 
   select(-cultivar) %>% 
   as.matrix %>% 
   princomp(cor=T)
tb.acp %>% summary
tb$Comp1 = tb.acp$score[,1]
tb$Comp2 = tb.acp$score[,2]

tb %>% 
  ggplot(aes(Comp1, Comp2, color=cultivar, shape=cultivar)) +
  geom_point() +
  ggtitle('ACP normée (princomp) italian wine dataset')
```

Analyse discriminante
```{r}
library(MASS)

model=lda(x=mat,grouping=y)
```

Commentez le contenu de `model`.

Comme il y a 3 classes, on peut projeter le résultat sur 2 axes (un de moins que le nombre de classes). Faire `plot(model)` et commenter.

LD1 et LD2 sont les vecteurs portant les axes de la projections (vecteurs propres associés aux plus grandes valeurs propres de V<sup>-1</sup>B avec V et B les matrices de variance-covariance totale et inter-classes respectivement, *cf.* cours).

Comparaison de la projection de l'ACP avec celle de la LDA :
```{r}
ld.proj <- scale( mat, center=T, scale=F ) %*% model$scaling %>% 
  as_tibble
tb <- tb %>% bind_cols(ld.proj)
ld.proj

library(gridExtra)
pl1 = tb %>% 
  ggplot(aes(x=-Comp1, y=Comp2, color=cultivar, shape=cultivar)) + 
  geom_point() + 
  theme(legend.position = "none") +
  ggtitle('ACP projection')
pl2 = tb %>%
  ggplot(aes(x=LD1, y=LD2, color=cultivar, shape=cultivar)) + 
  geom_point() + 
  theme(legend.position = "none") +
  ggtitle('LDA projection')
grid.arrange(pl1, pl2, ncol=2)
```


Est-ce qu'il est possible d'obtenir *a priori* de bonnes performances sur ce jeu de données avec une analyse discriminante ?

Pour l'analyse des performances, vous allez faire le schéma 2/3 *training* et 1/3 *test set* :
```{r}
n = nrow(mat)
training = sample(n, round(2*n/3)) # tirage aléatoire de 2/3 pour le jeu d'apprentissage
mat.train = mat[training,]
y.train = y[training]
mat.test = mat[-training,] # le reste pour le jeu de test
y.test = y[-training]
```

Effectuer l'analyse discriminante avec le jeu de données d'apprentissage ainsi obtenu et la prédiction sur le jeu de données test à l'aide de la fonction `predict(model, test)`. Commentez la sortie de la fonction `predict`.

Tracez le résultat avec `ggplot` :

![LDA](italian.wine.lda.png)

Selon l'échantillon de test tiré, il y aura ou non des individus mal classés : par exemple, un (M,V) apparaissant en carré vert sur le plot ci-dessus.

**Remarque :** la projection est sensiblement différente de la précédente puisque le jeu de données d'apprentissage est différent.

Calculer le taux d'erreurs pour votre tirage.


Vous pouvez utiliser la librairie `caret` pour produire la matrice de confusion :
```{r}
library(caret)
confusionMatrix(vecteur_classes_prédites, vecteur_classes_connues)
```

Utiliser les fonctions de `tidyverse` afin de construire cette matrice (notamment les fonctions `group_by`, `summarise` et `pivot_wider`).

A partir de cette matrice, si on a le temps, on pourra calculer pour chaque classe (connue), le nombre de P (*positive*), PP (*predicted positive*), N (*negative*), PN (*predicted negative*), TP (*true positive*), FP (*false positive*), TN (*true negative*), FN (*false negative*) et s'en servir ensuite pour ensuite la *precision*, le *recall*, le *f1 score* et le *Matthews correlation coefficient* (cf. https://docs.google.com/presentation/d/1bZsCqU4R2zFkBXaKkIjkyzYaqTXB47_2RpwbYh_wIz8/edit#slide=id.g11086d09bbd_0_14 pour les formules).

## Analyse discriminante pas-à-pas

### Rappels théoriques

**Objectif :** combinaison linéaire des variables permettant de séparer au mieux les groupes dans un nouvel espace de représentation

**Dispersion intra-groupes :** matrice de variance-covariance W<sub>k</sub> 

```math
W = \frac{1}{n} \sum_k n_k \times W_k
```
 avec n le nombre d'individus et n<sub>k</sub> le nombre d'individus de la classe k

Rappel : matrice de variance-covariance 
```math
(X - \overline{X})^T(X - \overline{X})/ n
```

**Eloignement entre les groupes :** matrice de variance-covariance inter-groupes B

```math
B = \frac{1}{n} \sum_k n_k (\mu_k -\mu)(\mu_k-\mu)^T
```

**Dispersion totale :** V = W + B

**Principe :** trouver les les axes factoriels qui maximisent l'éloignement des groupes par rapport à la dispersion totale

Z<sub>1</sub> premier axe factoriel associé au vecteur u<sub>1</sub> tel que l'on maximise 
```math
\frac{u_1^TBu_1}{u_1^TVu_1}
```

**Solution :** résolution de V<sup>-1</sup>Bu=&lambda;u

**Fonctions de R** qui pourront être utiles :

  * `by` : pour appliquer une fonction sur certaines lignes  d'une matrice selon leur classe (similaire à `tapply`)
  * `Reduce` : pour appliquer une opération entre chaque élément d'une liste (par exemple une somme ou un produit).

## Mise en oeuvre

Décomposer les calculs en R afin d'afficher la projection : Tout d'abord le calcul de W, puis de B, puis de V pour résoudre V<sup>-1</sup>Bu.

Pour faire la classification, il faut rechercher la classe qui maximise le score (adaptation de la distance de Mahalanobis = opposé du carré de cette distance).

Distance de Mahalanobis → distance d'un point au centre de gravité d'un ensemble de points en tenant compte de sa dispersion (S)
```math
d(x) = \sqrt{ (x-\mu)^T S^{-1} (x-\mu)}
```

Fonction de score 
```math
score(C=c, X=x) = -(X-\mu_c)^T W^{-1} (X - \mu_c)
```


## Annexes

Echantillon tiré aléatoirement par RB
```{r}
training.idx = c(74 , 23 , 43 , 88 , 67 , 7 , 81 , 111 , 73 , 51 , 16 , 55 , 33 , 141 , 58 , 96 , 1 , 103 , 101 , 106 , 143 , 164 , 64 , 157 , 2 , 42 , 92 , 155 , 176 , 83 , 153 , 120 , 110 , 66 , 146 , 125 , 140 , 48 , 85 , 56 , 119 , 168 , 14 , 104 , 128 , 77 , 72 , 135 , 80 , 79 , 118 , 169 , 78 , 142 , 114 , 117 , 19 , 145 , 122 , 39 , 175 , 124 , 171 , 174 , 24 , 138 , 136 , 112 , 6 , 109 , 148 , 137 , 12 , 84 , 8 , 17 , 172 , 152 , 154 , 3 , 47 , 160 , 35 , 94 , 97 , 156 , 173 , 49 , 30 , 165 , 177 , 41 , 10 , 18 , 21 , 87 , 54 , 158 , 139 , 163 , 91 , 127 , 121 , 150 , 159 , 4 , 149 , 129 , 134 , 89 , 99 , 29 , 90 , 131 , 60 , 95 , 65 , 105 , 34)
```

Erreurs ajoutées dans le jeu de test pour une matrice de confusion avec des FP, FN, ... non nuls
```{r}
y.test[c(25:26, 55:57)] = c('V','V', 'C','M','C')
```


<p>
<details>
<summary>Fonction pour les mesures de performances </summary>


```{r}
classification_performances=function(known_class, predicted_class) {
  tb.res = tibble(known_class, predicted_class) %>% 
    group_by(known_class, predicted_class) %>% 
    summarise(count=n(), .groups='keep') %>% 
    ungroup %>%
    arrange(known_class, predicted_class) %>%
    pivot_wider(names_from=predicted_class, values_from = count, values_fill=0)
  # reorder columns like rows
  tb.res = tb.res[ , c('known_class', as.character(tb.res$known_class))]
  # simple matrix for simple opreations (sum, rowSums, ...)
  confusion_matrix = tb.res %>% 
    select(-known_class) %>% 
    as.matrix
  # other computations
  tb.tmp = tibble(n_total = n_total, 
                  p  = rowSums(confusion_matrix), 
                  pp = colSums(confusion_matrix),
                  tp = diag(confusion_matrix)) %>%
     mutate(n = n_total-p, pn = n_total-pp,
      tp = tp, fp = pp-tp, fn = p-tp, tn = n_total-tp-fn-fp,
      sensibility = tp/p, specificity = tn/n, 
      precision = tp/(tp+fp), recall = tp/(tp+fn), 
      f1_score = 2*precision*recall/(precision+recall), 
      mcc = (tp*tn-fp*fn) / sqrt( (tp+fp)*(tp+fn)*(tn+fp)*(tn+fn)  )) 
  # returns all computed metrics
  tb.res %>% bind_cols(tb.tmp)
}

```
</details>
</p>