# TP Classification, k-nn, utilisation de sklearn et auto-skearn

Intro sklearn pour le prétraitmeent, la classification, la régression, le clustering, ...

Documentation → https://scikit-learn.org/stable/

Avant de commencer, se placer dans le projet datamining et récupérer les dernières mises à jour avec `git` :
```bash
cd datamining
git pull origin
cd tp.knn.jupyter.sklearn.autosklearn
```
si vous n'avez plus le projet :
```bash
git clone https://gitlab.com/rbarriot/datamining.git
cd datamining/tp.knn.jupyter.sklearn.autosklearn
```

puis lancement de `jupyter-lab`
```
conda activate fouille
jupyter-lab
```

Décomposition du TP en 2 parties

  * 1ère partie : sklearn et k plus proches voisins → [knn.sklearn.ipynb](knn.sklearn.ipynb) à utiliser dans jupyter-lab [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/rbarriot%2Fdatamining/master?filepath=tp.knn.jupyter.sklearn.autosklearn/knn.sklearn.ipynb)
  
  * 2ème partie : auto-sklearn ou la classification sans spécifier de méthode → à utiliser dans jupyter-lab [auto-sklearn.ipynb](auto-sklearn.ipynb) [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/rbarriot%2Fdatamining/master?filepath=tp.knn.jupyter.sklearn.autosklearn/auto-sklearn.ipynb)



