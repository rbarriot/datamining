# Devoir Maison de Classification à partir des données *Italian wine*

Ce devoir consiste à reproduire la méthodologie employée sur le jeu de données *mushrooms* sur un autre jeu de données. Il s'agit de mesures (quantitatives) effectuées sur 3 types de vins. La tâche de fouille de données consiste donc à tenter de prédire le type de vin en fonction des valeurs observées.

La méthodologie à reproduire est la suivante :

* Apprentissage du domaine d'application
* **Exploration** du jeu de données à disposition et **analyses préliminaires** à l'apprentissage automatique (en R)

* Nettoyage, prétraitement, réduction et transformation des données (quand c'est nécessaire)

* Méthodes de classification à mettre en oeuvre ainsi que méthodes d'évaluation et de comparaison des résultats obtenus
* **Mise en oeuvre** (avec Knime)

* **Evaluation** des performances et comparaisons des méthodes et/ou de leurs paramètres

Pour la première partie à réaliser en R, il s'agit donc d'explorer et d'analyser le jeu de données en amont de la tâche d'apprentissage automatique. Pour le travail à rendre, il s'agira notamment de fournir une analyse synthétique du contenu et de la pertinences des variables par rapport aux classes à prédire. Pour vous aider (notamment sur l'utilisation de `tidyverse` et `ggplot2`), un tutoriel vous est fourni dans le fichier [./italian.wine.basic.data.content.analysis.Rmd](./italian.wine.basic.data.content.analysis.Rmd) et sa sortie précompilée en [HTML](./italian.wine.basic.data.content.analysis.html) (à ouvrir après l'avoir rapatrié en local avec `git` pour voir le rendu).

Une fois ces analyses préliminaires réalisées, dans la deuxième partie, il s'agit d'utiliser Knime et de construire les *workflows* pour comparer les performances de différentes méthodes et de leurs paramètres, ou encore la manière de les évaluer. Pour cette partie, il vous donc faut réaliser l'évaluation des performances pour au moins 3 résultats obtenus en faisant varier les méthodes de classifications et/ou leurs paramètres et/ou les manières de mesurer les performances. 

Parmi les méthodes de classification vous pouvez notamment utiliser :

* des arbres de décision (*gain ratio* ou *gini index*) et/ou forêts aléatoies,
* des classificateurs bayésiens naïfs, 
* des réseaux de neuronnes, 
* les *k* plus proches voisins (pour certaines valeurs de *k*).

Pour l'évaluation des performances, vous avez le choix entre :

* partitionnement (2/3 apprentissage et 1/3 test ou encore 9/10 et 1/10, ...)
* validation croisée (3x ou 10x ou *leave one out* ou  ... avec un échantillonnage aléatoire ou linéaire, stratifié ou pas, *etc.*).

## Les données

Le jeu de données proposé concerne des vins italiens. Il est publié à l'adresse : http://archive.ics.uci.edu/ml/datasets/Wine

La description du jeu de données est aussi accessible dans le fichier [`italian.wine.variables.md`](./italian.wine.variables.md).

Le jeu de données a été modifié pour inclure une première ligne (contenant le nom des colonnes) correspond au fichier [`wine.data.csv`](../data/wine.data.csv).

## Travail à réaliser

Un rapport d'analyse incluant les analyses préliminaires en R sous forme synthétique (sélectionner les figures et résultats selon leur pertinence), puis le travail effectué avec Knime.

Le rapport peut être au format PDF "classique". Il peut aussi provenir de la compilation d'un RMarkdown (voire IPython Notebook si vous maîtrisez). Dans le deuxième cas, cela vous permet de rédiger le rapport directement dans le Rmd, ce qui peut-être pratique pour générer les figures sélectionnées. Par contre pour la partie "Knime", il vous faudra inclure des images, notamment celles illustrant les *workflows* construits, ainsi que des tableaux résumant les paramètres utilisés et les performances obtenues.
