# TP Classification, Classificateur bayésien naïf et prise en main de jupyter lab

L'objectif de ce TP est d'implémenter un script python permettant de faire de la classification automatique avec un modèle bayésien naïf. Pour ce faire, nous allons en même temps utiliser `jupyter lab` qui permet de faire des analyses interactives (un peu comme RStudio et Rmarkdown), de partager ces analyses , et de générer des rapports.

## Environnement

Pour ce TP, nous utiliserons l'environnement `conda` déjà créé et qui fournit `python`, `jupyter lab` ainsi que les modules pythons nécessaires. 

Pour le créer, il faut (**A NE PAS FAIRE SI L'ENVIRONNEMENT EXISTE DEJA**):
```bash
conda config --add channels bioconda 
conda create -n fouille python numpy pandas scipy scikit-learn matplotlib plotly ipykernel nb_conda_kernels jupyterlab
conda activate fouille
```

## Dernières mises à jour du projet `GitLab`

Pour récupérer la dernière version du projet, il faut se placer dans le shell dans la racine du projet puis :
```bash
cd datamining
git pull origin
```

Si vous n'avez pas ou plus le projet, il faut le récupérer :
```bash
git clone https://gitlab.com/rbarriot/datamining.git
cd datamining
```

## Lancement de `jupyter lab`

Dans un shell, il suffit de se placer dans le répertoire et d'exécuter la commande `jupyter-lab` qui va lancer un serveur Web et ouvrir la page correspondante :
```bash
conda activate fouille
cd datamining/tp.naive.bayes.jupyter-lab.pandas.script/
jupyter-lab
```

Une fois lancé, dans les fichiers disponibles depuis `jupyter-lab`, la suite du TP continue à partir du notebook python [naive.nayes.ipynb](tp.naive.bayes.jupyter-lab.pandas.script/naive.bayes.ipynb) [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/rbarriot%2Fdatamining/master?filepath=tp.naive.bayes.jupyter-lab.pandas.script/naive.bayes.ipynb).
